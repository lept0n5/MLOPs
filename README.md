# Project Name - Machine Learning MLOps Pipeline

This repository is dedicated to managing the machine learning project's code, data, and MLOps pipeline. It provides a structured framework for version control, automated testing, deployment, and monitoring of machine learning models.

Here's an overview of the structure of this repository:

- `.gitlab-ci.yml`: Configuration file for GitLab CI/CD pipeline.
- `Dockerfile`: Dockerfile for containerizing the machine learning model.
- `README.md`: This file, providing an overview of the repository.
- `requirements.txt`: List of project dependencies.
- `test_model.py`: Python script for unit tests.
- `train_model.py`: Python script to train the machine learning model.

## Version Control

This repository uses Git for version control. You can clone it using the following command:

```bash
git clone https://gitlab.com/lept0n5/MLOPs.git
```

You can commit your changes, create branches, and manage the project's history using Git.

## Docker Containerization

This section explains how to containerize your machine learning model and its dependencies using Docker.

### Prerequisites

Before you proceed, make sure you have Docker installed on your local machine.

### Dockerfile

In this project, we use a Dockerfile to define the environment and dependencies for running the machine learning model. Here's the Dockerfile:

```Dockerfile
# Use a base image with Python and other dependencies
FROM python:3.8

# Set working directory
WORKDIR /app

# Copy your Python script and dataset into the container
COPY train_model.py .

# Install Python dependencies
RUN pip install scikit-learn pandas

# Command to run your Python script
CMD ["python", "train_model.py"]
```

The Dockerfile specifies the base image, sets the working directory, copies your Python script and dataset into the container, installs necessary Python dependencies, and defines the command to run your Python script.

### Building the Docker Image

To build the Docker image, execute the following command in your project directory where the Dockerfile is located:

```bash
docker build -t lept0n5/mlops:1.0 .
```

Replace your-image-name with the desired name for your Docker image.

### Running the Docker Container

Once the Docker image is built, you can run the container with the following command:

```bash
docker run lept0n5/mlops:1.0
```

For more information on the Docker containerization process, refer to the [Dockerfile](Dockerfile) in this repository.

### Pushing the Docker Image to a Registry

To make your Docker image available for deployment on a cloud platform, you can push it to a container registry like Docker Hub. Here are the steps:

Step 1: Log in to Docker Hub (create an account if you don't have one):

```bash
docker login
```

Step 2: Tag your image with your Docker Hub username and image name:

```bash
docker tag lept0n5/mlops:1.0 lept0n5/mlops:1.0
```

Step 3: Push the image to Docker Hub:

```bash
docker push lept0n5/mlops:1.0 lept0n5/mlops:1.0
```

Now your Docker image is available on Docker Hub for deployment.# MLOPs

## MLOps Pipeline

The repository includes an MLOps pipeline that automates several tasks, including:

1. **Version Control**: Using Gitlab for managing code changes.

2. **Docker Containerization**: Building Docker images for the machine learning model.

3. **Cloud Deployment**: Deploying the Dockerized model to a cloud platform (e.g., AWS, Azure, or Google Cloud Platform).

4. **Automated Testing**: Running unit tests to ensure code quality and model performance.

5. **Monitoring and Logging**: Optional tools like Prometheus and Grafana for monitoring and logging.

## Automated Testing

The project includes a test suite to evaluate the performance and quality of the machine learning code. To run the tests, use the following command:

```bash
pip install -r requirements.txt
python test_model.py
```

## Deployment

The deployment stage is a critical component of this machine learning operations (MLOps) pipeline, where the goal is to make your trained machine learning model accessible and available for use in real-world applications. But I've no idea how to do it. I'm still learning. So, I couldn't do it. But I'll try to do it in the future.

### Steps in the Deployment Process

Step 1: **Infrastructure Setup**: To deploy your machine learning model, you typically begin by setting up the necessary infrastructure. This includes provisioning servers or cloud instances that will host your model. In your case, I've created an Amazon EC2 instance with an Ubuntu VM, which can serve as the deployment environment.

Step 2: **Establish the Connection to the instance**:

You can connect to the instance, in this case our vm, using ssh command(one way to do it). And log in to your system.

```bash
ssh -i "YOUR_INSTANCE_KEY.PEM" USERNAME@ipv4_public_ip 
```

or

```bash
ssh -i "YOUR_INSTANCE_KEY.PEM" USERNAME@ipv4_public_dns
```

Step 3: **Docker Image**: The Docker image, which I've built as part of the previous stage, plays a crucial role in the deployment process. It encapsulates the model, its dependencies, and the runtime environment. I just pulled the Docker image from Docker Hub and ran it on the EC2 instance.

To pull the dockerfile from DockerHUB:

```bash
docker pull <docker_image_name>
```

In our case:

```bash
docker pull lept0n5/mlops:1.0
```

And then you can run the image or you can clone the repository and build the image again.

Step 4: **Web Service or API**: I don't know how to do it. I'm still learning. So, I couldn't do it. But I'll try to do it in the future.

Step 5: **Integration, Scalability and Availability, Security, Monitoring and Logging**: No idea about these too. I'm still learning. So, I couldn't do it. But I'll try to do it in the future.
